var EntryRecogLog = require('../../model/entryRecogLog')

/** Function for adding an entry recognition log to the database.
 *  @param  {EntryRecogLog} model.
 *  @param  {Function} callback.
 *  @return {void}.
 */
module.exports.addEntryRecogLog = function(entryRecogLog, callback) {
  EntryRecogLog.create(entryRecogLog, callback);
};

/** Function for getting entry recognition logs from the database.
 *  @param  {Integer} restriction of number of entries to get.
 *  @param  {Function} callback.
 *  @return {void}.
 */
module.exports.getEntryRecogLogs = function(callback) {
  EntryRecogLog.find(callback); 
}

/** Function for getting entry recognition logs from the database by range of date.
 *  @param  {Integer} restriction of number of entries to get.
 *  @param  {Function} callback.
 *  @return {void}.
 */
module.exports.getEntryRecogLogsByRange = function(timestampStart, timestampEnd, callback) {
  EntryRecogLog.find({"date": {
                        $gte:new Date(timestampStart).toISOString(),
                        $lte:new Date(timestampEnd).toISOString()
                      }}, callback);
}

/** Function for getting number of entry recognition logs from the database by range of date.
 *  @param  {Integer} restriction of number of entries to get.
 *  @param  {Function} callback.
 *  @return {Number} how many entries.
 */
module.exports.getNumberEntryRecogLogsByRange = function(timestampStart, timestampEnd, callback) {
  EntryRecogLog.find({"date": {
                        $gte:new Date(timestampStart).toISOString(),
                        $lte:new Date(timestampEnd).toISOString()
                      }}, callback).count();
}

/** Function for getting entry recognition logs from the database which
 *  had face not recognized by the algorithm.
 *  @param  {Integer} restriction of number of entries to get.
 *  @param  {Function} callback.
 *  @return {void}.
 */
module.exports.getEntryRecogLogsNotRecognized = function(callback) {
  EntryRecogLog.find({'recognized': false}, callback)
               .sort({'dataOfUpdating': 'descending'});
}

/** Function for getting number of entry recognition logs from the database which
 *  had face not recognized by the algorithm.
 *  @param  {Integer} restriction of number of entries to get.
 *  @param  {Function} callback.
 *  @return {void}.
 */
module.exports.getNumberEntryRecogLogsNotRecognized = function(callback) {
  EntryRecogLog.find({'recognized': false}, callback).count();
}

/** Function for getting entry recognition logs from the database which
 *  had face recognized by the algorithm.
 *  @param  {Integer} restriction of number of entries to get.
 *  @param  {Function} callback.
 *  @return {void}.
 */
module.exports.getEntryRecogLogsRecognized = function(callback) {
  EntryRecogLog.find({'recognized': true}, callback)
               .sort({'dataOfUpdating': 'descending'});
}

/** Function for getting number of entry recognition logs from the database which
 *  had face recognized by the algorithm.
 *  @param  {Integer} restriction of number of entries to get.
 *  @param  {Function} callback.
 *  @return {void}.
 */
module.exports.getNumberEntryRecogLogsRecognized = function(callback) {
  EntryRecogLog.find({'recognized': true}, callback).count();
}
