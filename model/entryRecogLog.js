const mongoose = require('mongoose');

let entryRecogLogSchema = mongoose.Schema({
  // Unique ID is obtained when the function of insertion is utilized
  date: {
    type: Number, // Timestamp from the client
    required: true, // Needed trailing comma
  },
  dateIso: {
    type: String, // day/month_number/year
    required: true,
  },
  candidates: {
    type: [{
        type: Number, // Confidence
        required: true,
        }, {
        type: String, // PersonId
        required: true,
      }],
    required: true,
  },
  recognized: {
    type: Boolean,
    required: true,
  }
});

module.exports = EntryRecogLog = mongoose.model('entryRecogLog', entryRecogLogSchema);
