module.exports = class Db {
    constructor(type, host, port, container) {
      this.type = type;
      this.host = host;
      this.port = port;
      this.container = container;
    }
    setType(type) {
      this.type = type;
    }
    setHost(host) {
      this.host = host;
    }
    setPort(port) {
      this.port = port;
    }
    setContainer(container) {
      this.container = container;
    }
    getType() {
      return this.type;
    }
    getHost() {
      return this.host;
    }
    getPort() {
      return this.port;
    }
    getContainer() {
      return this.container;
    }
  }
  