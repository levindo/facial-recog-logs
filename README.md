# Facial Recognition Logs
REST Api responsible for managing logs from the facial recognition service.

__Author:__ Levindo Gabriel Taschetto Neto.

__Company:__ Realize CFI.

## Used Technologies

#### Framework(s)
* [ExpressJS v4.16.4](https://expressjs.com).

#### Parsing Middleware
* [Body-Parser v1.18.3](https://www.npmjs.com/package/body-parser).

#### Database
* [MongoDB](https://www.mongodb.com).

#### Object Modeling Tool
* [Mongoose v5.4.8](http://mongoosejs.com).

#### Linter
* [ESLint v5.12.1](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint).

## Requiremens for the deployment

### On Windows OS*
* [Docker for Windows](https://docs.docker.com/docker-for-windows/)

## How to Deploy and Start the Service with the Database

```terminal
$ docker-compose up --build
```

Thus, the app may be accessed on [http://localhost:PORT](http://localhost:PORT).
Where *PORT* can be configured [here](config/default.json) on *server.port*.

## How to use the Rest API

### Add an Entry Log (with an object element)
```terminal
POST HTTP/1.1
```
__URL:__ <HOST>:<PORT>/api/logs/recognition

##### Request body
```terminal
{
	"date": <Date>,
	"dateIso": <String>,
	"candidates": <List of Objects>,
	"recognized": <Boolean>
}
```

### GET Operations

#### Possible Status on Responses

* 202 OK (for registered logs).

### Get all Entry Logs

```terminal
GET HTTP/1.1
```
__URL:__ <HOST>:<PORT>/api/logs/recognition