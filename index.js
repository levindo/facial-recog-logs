const CONFIG = require('config');
const MONGOOSE = require('mongoose');
const BODY_PARSER = require('body-parser');
const EXPRESS = require('express');
const FUNCTIONS = require('./app/actions/recogFunctions');
const ENTRY_RECOG_LOG = require('./model/entryRecogLog');
const DATABASE = require('./model/database');
const PORT = CONFIG.get('server.port');
const APP = EXPRESS();
const STATUS = require('./constants/http-status');

APP.use(BODY_PARSER.json()); // Parse body input from the requests

const DB = new DATABASE(
  CONFIG.get('database.type'),
  CONFIG.get('database.host'),
  CONFIG.get('database.port'),
  CONFIG.get('database.container')
);

MONGOOSE.connect(
  DB.getType().concat('://', DB.getHost(),':', DB.getPort(), '/', DB.getContainer()),
  {useNewUrlParser: true}
).then(() => console.log('MongoDB Connected'))
.catch(error => console.log('Error connecting to the database:\n', error));

APP.listen(PORT, () => console.log('Listening on port '.concat(PORT)));
const DATABASE_CONN = MONGOOSE.connection; // Init db connection

APP.post('/api/logs/recognition', (request, response) => {
  const entryLog = new ENTRY_RECOG_LOG({
    date: request.body.date,
    dateIso: request.body.dateIso,
    candidates: request.body.candidates,
    recognized: request.body.recognized,
  });
  entryLog.save().then(() => response.json(entryLog));
});

APP.get('/api/logs/recognition', (request, response) => {
  FUNCTIONS.getEntryLogs((error, users) => {
      if (error) {
        throw error;
      } else {
        if(users.length == 0) {
          response.status(404).send('No entry found');
        } else {
            response.json(users);
        }
      }
  });
});

APP.get('/api/logs/recognition/range', (request, response) => {
  FUNCTIONS.getEntryRecogLogsByRange((error, users) => {
      if (error) {
        throw error;
      } else {
        if(users.length == 0) {
          response.status(STATUS.NOT_FOUND).send('No entry found');
        } else {
            response.json(users);
        }
      }
  });
});

APP.get('/api/logs/recognition/range/count', (request, response) => {
  FUNCTIONS.getNumberEntryRecogLogsByRange((error, users) => {
      if (error) {
        throw error;
      } else {
        if(users.length == 0) {
          response.status(STATUS.NOT_FOUND).send('No entry found');
        } else {
            response.json(users);
        }
      }
  });
});

APP.get('/api/logs/recognition/range/recog/count', (request, response) => {
  FUNCTIONS.getNumberEntryRecogLogsByRange((error, users) => {
      if (error) {
        throw error;
      } else {
        if(users.length == 0) {
          response.status(STATUS.NOT_FOUND).send('No entry found');
        } else {
            response.json(users);
        }
      }
  });
});

APP.get('/api/logs/recognition/range/recog/count', (request, response) => {
  FUNCTIONS.getNumberEntryRecogLogsByRange((error, users) => {
      if (error) {
        throw error;
      } else {
        if(users.length == 0) {
          response.status(STATUS.NOT_FOUND).send('No entry found');
        } else {
            response.json(users);
        }
      }
  });
});

APP.get('/api/logs/recognition/range/not_recog/', (request, response) => {
  FUNCTIONS.getEntryRecogLogsNotRecognized((error, users) => {
      if (error) {
        throw error;
      } else {
        if(users.length == 0) {
          response.status(STATUS.NOT_FOUND).send('No entry found');
        } else {
            response.json(users);
        }
      }
  });
});

APP.get('/api/logs/recognition/range/not_recog/count', (request, response) => {
  FUNCTIONS.getNumberEntryRecogLogsNotRecognized((error, users) => {
      if (error) {
        throw error;
      } else {
        if(users.length == 0) {
          response.status(STATUS.NOT_FOUND).send('No entry found');
        } else {
            response.json(users);
        }
      }
  });
});

APP.get('/api/logs/recognition/range/recog/', (request, response) => {
  FUNCTIONS.getEntryRecogLogsRecognized((error, users) => {
      if (error) {
        throw error;
      } else {
        if(users.length == 0) {
          response.status(STATUS.NOT_FOUND).send('No entry found');
        } else {
            response.json(users);
        }
      }
  });
});

APP.get('/api/logs/recognition/range/recog/count', (request, response) => {
  FUNCTIONS.getNumberEntryRecogLogsRecognized((error, users) => {
      if (error) {
        throw error;
      } else {
        if(users.length == 0) {
          response.status(STATUS.NOT_FOUND).send('No entry found');
        } else {
            response.json(users);
        }
      }
  });
});
